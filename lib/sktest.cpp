#include <algorithm>
#include <stdexcept>

#include "sktest.hpp"

extern "C" {
#include "curl/curl.h"
}

namespace sktest {


NetInspectorContext& NetInspectorContext::get()
{
    static NetInspectorContext inst;
    return inst;
}

NetInspectorContext::NetInspectorContext()
{
    curl_global_init(CURL_GLOBAL_DEFAULT);
}

NetInspectorContext::~NetInspectorContext()
{
    curl_global_cleanup();
}

NetInspector NetInspectorContext::inspector(std::string url)
{
    // Use return value optimisation to elide copy.
    return NetInspector(url);
}


// Write callback which does nothing.
static size_t null_writecb(char *ptr, size_t sz, size_t nmemb, void *userdata)
{
    (void) ptr; (void) userdata;
    // Do nothing.
    return sz * nmemb;
}


static double median(std::vector<double> v)
{
    size_t n = v.size();
    std::sort(v.begin(), v.end());
    if (n % 2)
    {
        // Odd case - pick the middle value
        return v[(n - 1) / 2];
    }
    else
    {
        // Even case - pick mean of two middle values
        // To avoid overflow, halve the two numbers first,
        // and round up if either of them was odd.
        double upper_mid = v[n / 2];
        double lower_mid = v[(n / 2) - 1];
        return (upper_mid/2) + (lower_mid/2);
    }
}


class NetInspector::Impl {
public:
    Impl(std::string url) : m_url(url), m_curl(curl_easy_init()), m_errbuf(), m_headers(nullptr)
    {
        // Ensure curl instance constructed okay
        if(!m_curl)
        {
            throw std::runtime_error("curl init failed");
        }
        // Set up error buffer for useful messages
        curl_easy_setopt(m_curl, CURLOPT_ERRORBUFFER, m_errbuf.data());
        // Give curl the URL requested
        CURLcode err = curl_easy_setopt(m_curl, CURLOPT_URL, m_url.c_str());
        if (err != CURLE_OK)
        {
            // Can't use m_errbuf for exception message here, as it will be
            // destructed when we abort the constructor!
            throw std::runtime_error("url invalid");
        }
        err = curl_easy_setopt(m_curl, CURLOPT_HTTPGET, 1);
        if (err != CURLE_OK)
        {
            // Can't use m_errbuf for exception message here, as it will be
            // destructed when we abort the constructor!
            throw std::runtime_error("cannot set get request");
        }
        err = curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, null_writecb);
        if (err != CURLE_OK)
        {
            // Can't use m_errbuf for exception message here, as it will be
            // destructed when we abort the constructor!
            throw std::runtime_error("cannot set write function");
        }
    }

    void get(void)
    {
        // Perform the request and raise exception to caller if necessary
        CURLcode err = curl_easy_perform(m_curl);
        if (err != CURLE_OK)
        {
            throw std::runtime_error(m_errbuf.data());
        }

        // Collect performance data from handle
        double out = 0;
        err = curl_easy_getinfo(m_curl, CURLINFO_NAMELOOKUP_TIME, &out);
        if (err != CURLE_OK)
        {
            throw std::runtime_error(m_errbuf.data());
        }
        m_namelookup_times.push_back(out);
        err = curl_easy_getinfo(m_curl, CURLINFO_CONNECT_TIME, &out);
        if (err != CURLE_OK)
        {
            throw std::runtime_error(m_errbuf.data());
        }
        m_connect_times.push_back(out);
        err = curl_easy_getinfo(m_curl, CURLINFO_STARTTRANSFER_TIME, &out);
        if (err != CURLE_OK)
        {
            throw std::runtime_error(m_errbuf.data());
        }
        m_starttransfer_times.push_back(out);
        err = curl_easy_getinfo(m_curl, CURLINFO_TOTAL_TIME, &out);
        if (err != CURLE_OK)
        {
            throw std::runtime_error(m_errbuf.data());
        }
        m_total_times.push_back(out);
    }

    /// Add some headers
    void add_headers(std::vector<std::string> hdrs)
    {
        struct curl_slist *new_hdrs = nullptr;
        for (const auto& hdr: hdrs)
        {
            new_hdrs = curl_slist_append(new_hdrs, hdr.c_str());
            if (new_hdrs == nullptr)
            {
                throw std::runtime_error("header write failed");
            }
        }

        if (m_headers != nullptr)
        {
            curl_slist_free_all(m_headers);
        }
        m_headers = new_hdrs;
        CURLcode err = curl_easy_setopt(m_curl, CURLOPT_HTTPHEADER, m_headers);
        if (err != CURLE_OK)
        {
            throw std::runtime_error(m_errbuf.data());
        }
    }

    /// Get the server IP address
    ///
    /// The returned pointer has lifetime of the Impl.
    const char *ip_address(void)
    {
        char *ip;
        CURLcode err = curl_easy_getinfo(m_curl, CURLINFO_PRIMARY_IP, &ip);
        if (err != CURLE_OK || (ip == nullptr))
        {
            throw std::runtime_error(m_errbuf.data());
        }
        return ip;
    }

    /// Get the HTTP response code
    long response_code(void)
    {
        long out = 0;
        CURLcode err = curl_easy_getinfo(m_curl, CURLINFO_RESPONSE_CODE, &out);
        if (err != CURLE_OK)
        {
            throw std::runtime_error(m_errbuf.data());
        }
        return out;
    }

    /// Get the median name lookup time
    double median_namelookup_time(void)
    {
        return median(m_namelookup_times);
    }

    /// Get the median connect time
    double median_connect_time(void)
    {
        return median(m_connect_times);
    }

    /// Get the median starttransfer time
    double median_starttransfer_time(void)
    {
        return median(m_starttransfer_times);
    }

    /// Get the median total time
    double median_total_time(void)
    {
        return median(m_total_times);
    }

    ~Impl()
    {
        if (m_headers != nullptr)
        {
            curl_slist_free_all(m_headers);
        }

        curl_easy_cleanup(m_curl);
    }

private:
    std::string m_url;
    CURL *m_curl;
    std::vector<char> m_errbuf;
    std::vector<double> m_namelookup_times;
    std::vector<double> m_connect_times;
    std::vector<double> m_starttransfer_times;
    std::vector<double> m_total_times;
    struct curl_slist *m_headers;
};


// The outer constructor and destructor are needed in this compilation unit so
// that std::vector in m_pimpl can get the sizeof for the Impl class.
NetInspector::NetInspector(std::string url) : m_pimpl(std::make_unique<Impl>(url)) { }

NetInspector::~NetInspector() { }

void NetInspector::get(void)
{
    m_pimpl->get();
}

void NetInspector::add_headers(std::vector<std::string> hdrs)
{
    return m_pimpl->add_headers(hdrs);
}

const char *NetInspector::ip_address(void)
{
    return m_pimpl->ip_address();
}

long NetInspector::response_code(void)
{
    return m_pimpl->response_code();
}

double NetInspector::median_namelookup_time(void)
{
    return m_pimpl->median_namelookup_time();
}

double NetInspector::median_connect_time(void)
{
    return m_pimpl->median_connect_time();
}

double NetInspector::median_starttransfer_time(void)
{
    return m_pimpl->median_starttransfer_time();
}

double NetInspector::median_total_time(void)
{
    return m_pimpl->median_total_time();
}



}
