#include <memory>
#include <string>
#include <vector>


#ifndef INCLUDE_SKTEST_HPP
#define INCLUDE_SKTEST_HPP

namespace sktest {

class NetInspector;

///
/// The main interface to the sktest library - use to open a session, perform
/// tests and inspect results. This must be accessed in a single-threaded
/// environment before starting multiple threads, but can be used to create
/// new NetInspector objects throughout the program.
///
/// The lifetime of this object must last beyond all of the NetInspector
/// instances created with it.
///
class NetInspectorContext {
public:
    static NetInspectorContext& get();
    ~NetInspectorContext();

    /// Create and return a NetInspector instance.
    ///
    /// It must not outlive the context that created it.
    NetInspector inspector(std::string url);

    // Deleted constructors to prevent copying/moving.
    NetInspectorContext(const NetInspectorContext&) = delete;
    NetInspectorContext& operator=(const NetInspectorContext&) = delete;
    NetInspectorContext(NetInspectorContext&&) = delete;
    NetInspectorContext& operator=(NetInspectorContext&&) = delete;

private:
    NetInspectorContext();
};

/// The NetInspector is used for carrying out requests to the desired server
/// and reporting on the statistics accumulated while performing the same.
///
/// It can only be constructed from a NetInspectorContext.
class NetInspector {
public:
    class Impl;
    friend NetInspectorContext;
    ~NetInspector();

    /// Perform one get request to the server
    void get(void);

    /// Add custom headers
    void add_headers(std::vector<std::string>);

    /// Get the server IP address
    ///
    /// The returned pointer has lifetime of the NetInspector.
    const char *ip_address(void);

    /// Get the serer response code
    long response_code(void);

    /// Get the median name lookup time
    double median_namelookup_time(void);

    /// Get the median connect time
    double median_connect_time(void);

    /// Get the median starttransfer time
    double median_starttransfer_time(void);

    /// Get the median total time
    double median_total_time(void);

    // Deleted constructors to prevent copying/moving.
    NetInspector(const NetInspector&) = delete;
    NetInspector& operator=(const NetInspector&) = delete;
    NetInspector(NetInspector&&) = delete;
    NetInspector& operator=(NetInspector&&) = delete;

protected:
    NetInspector(std::string url);

private:
    std::unique_ptr<Impl> m_pimpl;
};
}

#endif
