#include <iostream>

#include "cxxopts.hpp"

#include "sktest.hpp"
#include "output_fmt.hpp"


int main(int argc, char* argv[])
{
    cxxopts::Options options("sktest", "test connection to a website");
    options.add_options()
        ("n,number", "number of requests", cxxopts::value<size_t>()->default_value("100"))
        ("H,header", "headers to add", cxxopts::value<std::vector<std::string>>())
        ("u,url", "url to request", cxxopts::value<std::string>()->default_value("http://www.google.com"))
        ("h,help", "print usage")
    ;
    auto result = options.parse(argc, argv);

    if (result.count("help"))
    {
        std::cerr << options.help() << std::endl;
        exit(0);
    }
    std::vector<std::string> headers;
    if (result.count("header"))
    {
        headers = result["header"].as<std::vector<std::string>>();
    }
    std::string url = result["url"].as<std::string>();
    size_t n_iter = result["number"].as<size_t>();

    // The main context - to be kept alive throught the program.
    auto& ctx = sktest::NetInspectorContext::get();
    auto inspector = ctx.inspector(url);
    if (headers.size())
    {
        inspector.add_headers(headers);
    }

    for (size_t i = 0; i < n_iter; i++)
    {
        inspector.get();
    }

    skapp::OutputFormatter of(inspector);
    of.output(std::cout);
    return 0;
}
