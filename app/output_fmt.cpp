
#include "output_fmt.hpp"


namespace skapp {

OutputFormatter::OutputFormatter(sktest::NetInspector& ni) : m_ni(ni) {
    // Nothing to do.
}


void OutputFormatter::output(std::ostream& o)
{
    o << "SKTEST;"
        << m_ni.ip_address() << ";"
        << m_ni.response_code() << ";"
        << m_ni.median_namelookup_time() << ";"
        << m_ni.median_connect_time() << ";"
        << m_ni.median_starttransfer_time() << ";"
        << m_ni.median_total_time() << std::endl;
}

}