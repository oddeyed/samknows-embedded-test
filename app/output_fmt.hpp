
#include <iostream>

#include "sktest.hpp"

#ifndef INCLUDE_OUTPUT_FMT
#define INCLUDE_OUTPUT_FMT

namespace skapp {

class OutputFormatter {
public:
    OutputFormatter(sktest::NetInspector&);
    void output(std::ostream&);

private:
    sktest::NetInspector& m_ni;
};

}

#endif
