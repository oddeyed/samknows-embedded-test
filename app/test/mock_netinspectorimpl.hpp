#include <sktest.hpp>

namespace sktest {

class NetInspector::Impl
{
public:
    static const char* ip_address;
    static long response_code;
    static double median_name_lookup_time;
    static double median_connect_time;
    static double median_starttransfer_time;
    static double median_total_time;
};

}