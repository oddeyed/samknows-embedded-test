#include "output_fmt.hpp"

#include "sktest.hpp"
#include "mock_netinspectorimpl.hpp"

#include <gtest/gtest.h>

// Demonstrate some basic assertions.
TEST(OutputFormatter, SimpleCases) {
    auto& ctx = sktest::NetInspectorContext::get();
    auto inspector = ctx.inspector("http://example.com");
    skapp::OutputFormatter output{inspector};

    sktest::NetInspector::Impl::ip_address = "192.168.1.1";
    sktest::NetInspector::Impl::response_code = 200;
    sktest::NetInspector::Impl::median_name_lookup_time = 1234;
    sktest::NetInspector::Impl::median_connect_time = 2345;
    sktest::NetInspector::Impl::median_starttransfer_time = 3456;
    sktest::NetInspector::Impl::median_total_time = 4567;

    std::string expected = "SKTEST;192.168.1.1;200;1234;2345;3456;4567\n";

    testing::internal::CaptureStdout();
    output.output(std::cout);
    std::string received = testing::internal::GetCapturedStdout();
    EXPECT_EQ(expected, received);
}
