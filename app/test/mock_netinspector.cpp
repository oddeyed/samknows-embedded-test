#include "sktest.hpp"
#include "mock_netinspectorimpl.hpp"

namespace sktest {

NetInspectorContext& NetInspectorContext::get()
{
    static NetInspectorContext inst;
    return inst;
}

NetInspectorContext::NetInspectorContext() { }

NetInspectorContext::~NetInspectorContext() { }

NetInspector NetInspectorContext::inspector(std::string url)
{
    return NetInspector(url);
}

/// Custom implementation of NetInspector for tests
void NetInspector::get(void)
{

}

NetInspector::NetInspector(std::string) { }

NetInspector::~NetInspector() { }

const char *NetInspector::ip_address(void)
{
    return m_pimpl->ip_address;
}

long NetInspector::response_code(void)
{
    return m_pimpl->response_code;
}

double NetInspector::median_namelookup_time(void)
{
    return m_pimpl->median_name_lookup_time;
}

double NetInspector::median_connect_time(void)
{
    return m_pimpl->median_connect_time;
}

double NetInspector::median_starttransfer_time(void)
{
    return m_pimpl->median_starttransfer_time;
}

double NetInspector::median_total_time(void)
{
    return m_pimpl->median_total_time;
}


// Define static storage for mock variables
const char* NetInspector::Impl::ip_address;
long  NetInspector::Impl::response_code;
double  NetInspector::Impl::median_name_lookup_time;
double  NetInspector::Impl::median_connect_time;
double  NetInspector::Impl::median_starttransfer_time;
double  NetInspector::Impl::median_total_time;

}