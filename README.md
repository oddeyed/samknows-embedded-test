# SamKnows Test Submission

## Pre-requisites

Ensure you have CMake and a suitable toolchain (e.g. ninja, Visual Studio, make) installed. You will also need the curl libraries, which are installed on Fedora using `sudo dnf install libcurl-devel`.

## Build Instructions

Use CMake to build your project, then build the project. For example:

```bash
$ mkdir build
$ cd build
$ cmake ..
-- The CXX compiler identification is GNU 11.2.1
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Found CURL: /usr/lib64/libcurl.so (found version "7.79.1")
-- Configuring done
-- Generating done
-- Build files have been written to: /home/ben/Documents/Code/samknows-test/build
$ make
[ 25%] Building CXX object lib/CMakeFiles/sktestLib.dir/sktest.cpp.o
[ 50%] Linking CXX static library libsktestLib.a
[ 50%] Built target sktestLib
[ 75%] Building CXX object app/CMakeFiles/sktestApp.dir/app.cpp.o
[100%] Linking CXX executable sktestApp
[100%] Built target sktestApp
```

And then it can be run with:

```
$ ./app/sktestApp
```

## Test(s)

Google Test is used for the unit test(s). To run them, run

```
ctest
```

after having built with make.

## Licences

The application uses cxxopts for command line parsing, which is licenced under the MIT licence:

```
Copyright (c) 2014 Jarryd Beck

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```